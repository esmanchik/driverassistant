package pl.jesmanczyk.android.driverassistant;

import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.Preference.OnPreferenceClickListener;
import android.content.SharedPreferences;
import android.widget.Toast;

import java.io.File;
import java.lang.ref.Reference;

public class SettingsActivity extends PreferenceActivity {
    private Preference clearPreference;
    private OnPreferenceClickListener clearPreferenceListener;
    private Preference openMapPrefernce;
    private OnPreferenceClickListener openMapPrefernceListrner;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_general);

        clearPreference = (Preference) findPreference("clear_history");
        clearPreference.setOnPreferenceClickListener(clearPreferenceListener = new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                DetectionRepository.clearHistory();
                Toast.makeText(SettingsActivity.this,"History is cleaned",Toast.LENGTH_SHORT);
                return true;
            }
        });
        openMapPrefernce = (Preference) findPreference("open_map");
        openMapPrefernce.setOnPreferenceClickListener(openMapPrefernceListrner = (OnPreferenceClickListener)(preference) -> {
            Intent intent = new Intent(SettingsActivity.this, MapsActivity.class);
            startActivity(intent);
            return true;
        });
    }
}

