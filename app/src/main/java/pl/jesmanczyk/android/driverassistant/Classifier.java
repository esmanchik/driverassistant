package pl.jesmanczyk.android.driverassistant;

import android.content.res.AssetManager;
import android.graphics.Bitmap;

import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Classifier {
    public static final String MODEL_FILE = "file:///android_asset/classifier_graph.pb";
    private static final String LABEL_FILE =
            "file:///android_asset/classifier_labels.txt";

    private TensorFlowInferenceInterface inception;
    private List<String> labels;
    private static final int inputSize = 224;
    private static final float imageMean = 0f; // 117f;
    private static final float imageStd = 255f; // 1f;
    private int[] intValues;
    private float[] floatValues;
    private static String[] outputNames = new String[]{"final_result"};
    private float[] output;


    public Classifier(AssetManager assetManager) {
        inception = new TensorFlowInferenceInterface(assetManager, MODEL_FILE);
        labels = new ArrayList<>();
        String labelFilename = LABEL_FILE;
        String actualFilename = labelFilename.split("file:///android_asset/")[1];
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(assetManager.open(actualFilename)));
            String line;
            while ((line = br.readLine()) != null) {
                labels.add(line);
            }
            br.close();
        } catch (IOException e) {
            throw new RuntimeException("Problem reading label file!" , e);
        }
        intValues = new int[inputSize * inputSize];
        floatValues = new float[inputSize * inputSize * 3];
        output = new float[labels.size()];

    }

    public String classify(Bitmap cut) {
        Detector.Crop inceptionReshaper = new Detector.Crop(inputSize);
        Bitmap inceptionBitmap = inceptionReshaper.apply(cut);
        inceptionBitmap.getPixels(intValues, 0, inceptionBitmap.getWidth(), 0, 0,
                inceptionBitmap.getWidth(), inceptionBitmap.getHeight());
        for (int i = 0; i < intValues.length; ++i) {
            final int val = intValues[i];
            floatValues[i * 3 + 0] = (((val >> 16) & 0xFF) - imageMean) / imageStd;
            floatValues[i * 3 + 1] = (((val >> 8) & 0xFF) - imageMean) / imageStd;
            floatValues[i * 3 + 2] = ((val & 0xFF) - imageMean) / imageStd;
        }
        inception.feed("input", floatValues, 1, inputSize, inputSize, 3);
        inception.run(outputNames);
        inception.fetch(outputNames[0], output);
        int m = 0;
        float max = output[0];
        for (int i = 0; i < output.length; ++i) {
            if (max < output[i]) {
                max = output[i];
                m = i;
            }
        }
        return labels.get(m);
    }
}
