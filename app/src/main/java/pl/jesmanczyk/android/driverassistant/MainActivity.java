package pl.jesmanczyk.android.driverassistant;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.PointF;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import org.tensorflow.demo.env.Logger;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import pl.jesmanczyk.android.driverassistant.DetectionRepository.DetectionFrame;

import static pl.jesmanczyk.android.driverassistant.DetectionRepository.Detection;

public class MainActivity extends AppCompatActivity implements Camera.PreviewCallback,
        Vision.ImageRotationAware, TextToSpeech.OnUtteranceCompletedListener {
    private static final Logger LOGGER = new Logger();

    private TextView label;
    private TextureView textureView;
    private TextureView.SurfaceTextureListener surfaceTextureListener;
    private Camera camera;
    private HandlerThread backgroundThread;
    private Handler handler;
    private AtomicBoolean isProcessingFrame;
    private AtomicBoolean isSpeaking;
    private static final int DETECTION_COMPLETED = 1;
    private static final int ERROR_OCCURED = 0;
    private Handler mainHandler;
    private ImageAdapter detectionHistoryViewAdapter;
    private Button settingsControl;
    private boolean refine;
    private TextToSpeech tts;
    private boolean useTts;
    private Set<String> skipLabels;
    private int minCameraFrameHeight;
    private boolean saveHistory;
    private Vision vision;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private AtomicReference<Location> currentLocation;

    private GridView detectionHistoryView;
    private DetectionRepository detectionRepository;

    private AdView adView;
    private float detectionThreshold;
    private InterstitialAd interstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        MobileAds.initialize(this, "ca-app-pub-2023178485743357~3468169709");
        String maintertitial = "ca-app-pub-2023178485743357/6931062714"; // prod
        // String maintertitial = "ca-app-pub-3940256099942544/1033173712"; // test
        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(maintertitial);
        interstitialAd.loadAd(new AdRequest.Builder().build());
        adView = (AdView) findViewById(R.id.adView);
        if (adView != null) {
            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice("BA5A4FC98CF7307B6C135399D447F313").build();
            adView.loadAd(adRequest);
        }

        init();

        if (!hasPermission()) {
            requestPermission();
            return;
        }

        try {
            detectionRepository.load();
        } catch (Throwable e) {
            String message = "Failed to load detection history";
            //LOGGER.e(e, message);
            label.setText(message + " " + e.getMessage());
        }

    }


    @Override
    protected void onStart() {
        super.onStart();
        label.setText(ImageUtils.stringFromJNI());
    }

    @Override
    protected void onStop() {
        if (saveHistory) {
            try {
                //detectionRepository.save();
            } catch (Throwable e) {
                //LOGGER.e(e, "Failed to save detection history");
            }
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void error(String message, Throwable e) {
        String failure = "Failed to " + message;
        //LOGGER.e(e, failure);
        String caused = failure + " due to " + e;
        label.setText(caused);
        Toast.makeText(MainActivity.this, caused, Toast.LENGTH_LONG);
        Crashlytics.logException(e);
    }

    private void init() {
        readPreferences();
        label = (TextView) findViewById(R.id.sample_text);

        textureView = (TextureView) findViewById(R.id.textureView);
        isProcessingFrame = new AtomicBoolean(false);
        textureView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(MainActivity.this, MapsActivity.class);
                //startActivity(intent);
            }
        });

        settingsControl = (Button) findViewById(R.id.settingsButton);
        settingsControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(intent);
            }
        });

        detectionRepository = new DetectionRepository();
        detectionHistoryViewAdapter = new ImageAdapter(this, detectionRepository);
        detectionHistoryView = (GridView) findViewById(R.id.detectionsGridView);
        detectionHistoryView.setAdapter(detectionHistoryViewAdapter);

        mainHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message inputMessage) {
                switch (inputMessage.what) {
                    case DETECTION_COMPLETED:
                        DetectionFrame frame = (DetectionFrame) inputMessage.obj;
                        if (frame.detections.size() > 0) {
                            Location lastKnownLocation = currentLocation.get();
                            if (lastKnownLocation != null) {
                                for (Detection detection : frame.detections) {
                                    detection.gps = new DetectionRepository.GpsCoords();
                                    detection.gps.latitude = lastKnownLocation.getLatitude();
                                    detection.gps.longitude = lastKnownLocation.getLongitude();
                                }
                            }
                            detectionHistoryViewAdapter.addFrame(frame);
                            StringBuilder toSpeak = new StringBuilder();// + frame.detections.size() + " objects";
                            for (Detection detection : frame.detections) {
                                toSpeak.append(" ").append(detection.label);
                                if (detection.label.equals("prohibitory") ||
                                        detection.label.equals("priority")) toSpeak.append(" sign");
                            }
                            if (useTts) {
                                HashMap<String, String> speechParams = new HashMap<>();
                                speechParams.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, toSpeak.toString());
                                //if (!isSpeaking.getAndSet(true))
                                //tts.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, speechParams);
                                tts.speak(toSpeak.toString(), TextToSpeech.QUEUE_ADD, speechParams);
                            }
                            if (interstitialAd.isLoaded() && detectionHistoryViewAdapter.getCount() > 4) {
                                interstitialAd.show();
                            } else {
                                Log.d("MainActivity", "The interstitial wasn't loaded yet.");
                            }
                        }
                        //label.setText("Detected " + frame.detections.size() + " objects, stored " + detectionRepository.getHistorySize());
                        break;
                    case ERROR_OCCURED:
                        Throwable e = (Throwable) inputMessage.obj;
                        error("perform backgroud task", e);
                        break;
                    default:
                        super.handleMessage(inputMessage);
                }
            }
        };

        isSpeaking = new AtomicBoolean(false);
        tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.ERROR) {
                    tts = null;
                } else {
                    tts.setLanguage(Locale.ENGLISH);
                    tts.setOnUtteranceCompletedListener(MainActivity.this);
                }
            }
        });

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        currentLocation = new AtomicReference<>(null);
        // Define a listener that responds to location updates
        locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
                Location current = currentLocation.get();
                if (isBetterLocation(location, current)) {
                    currentLocation.set(location);
                }
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };


        surfaceTextureListener = new TextureView.SurfaceTextureListener() {
            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture texture, int width, int height) {
                startCamera();
            }

            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
                camera.stopPreview();
                Camera.Parameters parameters = camera.getParameters();
                CameraParametersAdapter cameraParametersAdapter = CameraParametersAdapter.fromCameraParameters(parameters);
                cameraParametersAdapter = CameraParametersAdapter.calcImageSize(
                        cameraParametersAdapter, getDisplayOrientation(), minCameraFrameHeight
                );
                camera.setParameters(parameters);
                camera.startPreview();
            }

            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                if (camera != null) {
                    //camera.stopPreview();
                    //camera.release();
                }
                return true;
            }

            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surface) {

            }
        };
    }

    @Override
    public void onResume() {
        super.onResume();
        readPreferences();
        try {
            if (vision == null) {
                vision = new Vision(getAssets(), this);
            }
            vision.setDetectionThreshold(detectionThreshold)
                    .setSkipLabels(skipLabels).setRefine(refine);
        } catch (Throwable e) {
            error(vision == null ? "create vision" : "update vision", e);
        }
        startBackgroundThread();
        if (!hasPermission()) {
            return;
        }
        resumeCamera();
        startLocationUpdates();
    }

    @Override
    public void onPause() {
        stopLocationUpdates();
        stopCamera();
        stopBackgroundThread();
        super.onPause();
    }

    @Override
    public void onUtteranceCompleted(String s) {
        isSpeaking.set(false);
    }

    private void readPreferences() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        try {
            minCameraFrameHeight = Integer.parseInt(preferences.getString("min_frame_height", "400"));
        } catch (NumberFormatException e) {
            minCameraFrameHeight = 400;
        }
        try {
            detectionThreshold = Float.parseFloat(preferences.getString("detection_threshold", "0.2"));
        } catch (NumberFormatException e) {
            detectionThreshold = 0.2f;
        }
        skipLabels = preferences.getStringSet("skip_label_list",
                new HashSet<String>(Arrays.asList("no heavy goods vehicles")));
        refine = preferences.getBoolean("example_switch", true);
        saveHistory = preferences.getBoolean("save_history", true);
        useTts = preferences.getBoolean("use_tts", true);
    }

    /**
     * Starts a background thread and its {@link Handler}.
     */
    private synchronized void startBackgroundThread() {
        backgroundThread = new HandlerThread("CameraBackground");
        backgroundThread.start();
        handler = new Handler(backgroundThread.getLooper());
    }

    /**
     * Stops the background thread and its {@link Handler}.
     */
    private synchronized void stopBackgroundThread() {
        backgroundThread.quitSafely();
        try {
            backgroundThread.join();
            backgroundThread = null;
            handler = null;
        } catch (final InterruptedException e) {
            //LOGGER.e(e, "Exception!");
        }
    }

    protected synchronized void runInBackground(final Runnable r) {
        if (handler != null) {
            handler.post(r);
        } else {
            //LOGGER.e("Background handler is null");
        }
    }

    private void startLocationUpdates() {
        // Register the listener with the Location Manager to receive location updates
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
    }

    private void stopLocationUpdates() {
        locationManager.removeUpdates(locationListener);
    }

    private void startCamera() {
        int camId = getCameraId();
        if (camId < 0) return;
        try {
            camera = Camera.open(camId);
            Camera.Parameters parameters = camera.getParameters();
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            CameraParametersAdapter cameraParametersAdapter = CameraParametersAdapter.fromCameraParameters(parameters);
            cameraParametersAdapter = CameraParametersAdapter.calcImageSize(
                    cameraParametersAdapter, getDisplayOrientation(), minCameraFrameHeight
            );
            camera.setParameters(parameters);
            Camera.Size s = parameters.getPictureSize();
            resolutionChanged(s.width, s.height);
            int orientation = getDisplayOrientation();
            camera.setDisplayOrientation(orientation);
            camera.setPreviewTexture(textureView.getSurfaceTexture());
            //camera.setPreviewCallback(this);
            camera.setPreviewCallbackWithBuffer(this);
            camera.addCallbackBuffer(new byte[ImageUtils.getYUVByteSize(s.height, s.width)]);
            camera.startPreview();
        } catch (Throwable e) {
            error("open camera " + camId, e);
        }
    }

    private void resumeCamera() {
        // When the screen is turned off and turned back on, the SurfaceTexture is already
        // available, and "onSurfaceTextureAvailable" will not be called. In that case, we can open
        // a camera and start preview from here (otherwise, we wait until the surface is ready in
        // the SurfaceTextureListener).
        if (textureView.isAvailable()) {
            startCamera();
        } else {
            textureView.setSurfaceTextureListener(surfaceTextureListener);
        }
    }

    private void stopCamera() {
        if (camera != null) {
            camera.stopPreview();
            camera.setPreviewCallback(null);
            camera.release();
            camera = null;
        }
    }

    private int getCameraId() {
        Camera.CameraInfo ci = new Camera.CameraInfo();
        for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
            Camera.getCameraInfo(i, ci);
            if (ci.facing == Camera.CameraInfo.CAMERA_FACING_BACK)
                return i;
        }
        return -1; // No camera found
    }

    private Camera.CameraInfo getCameraInfo() {
        Camera.CameraInfo ci = new Camera.CameraInfo();
        for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
            Camera.getCameraInfo(i, ci);
            if (ci.facing == Camera.CameraInfo.CAMERA_FACING_BACK)
                return ci;
        }
        return ci; // No camera found
    }

    @Override
    public int getImageRotation() {
        return getDisplayOrientation();
    }

    private int getDisplayOrientation() {
        return getCameraDisplayOrientation(getCameraInfo());
    }

    private int getCameraDisplayOrientation(Camera.CameraInfo info) {
        int rotation = getWindowManager().getDefaultDisplay()
                .getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        return result;
    }

    private void resolutionChanged(int width, int height) {
        try {
            vision.imageSizeChanged(new Vision.Size(width, height));
            label.setText("Camera resolution is " + width + " x " + height);
        } catch (final Exception e) {
            error("set camera buffers", e);
        }
    }

    @Override
    public void onPreviewFrame(final byte[] bytes, final Camera camera) {
        if (vision == null) return;

        if (isProcessingFrame.getAndSet(true)) {
            //LOGGER.w("Dropping bitmap!");
            return;
        }

        runInBackground(new Runnable() {
            @Override
            public void run() {
                try {
                    DetectionFrame frame = vision.look(bytes);
                    mainHandler.obtainMessage(DETECTION_COMPLETED, frame).sendToTarget();
                } catch (Throwable e) {
                    mainHandler.obtainMessage(ERROR_OCCURED, e).sendToTarget();
                }
                camera.addCallbackBuffer(bytes);
                isProcessingFrame.set(false);
            }
        });
    }

    public static class ImageAdapter extends BaseAdapter {
        private MainActivity context;
        private DetectionRepository history;

        public ImageAdapter(MainActivity c, DetectionRepository bs) {
            context = c;
            history = bs;
        }

        public int getCount() {
            return history.getHistorySize();
        }

        public void addFrame(DetectionFrame frame) {
            history.addFrame(frame, context.saveHistory);
            notifyDataSetChanged();
        }

        public void addAll(Collection<Detection> detections) {
            history.addFrameDetections(detections);
            notifyDataSetChanged();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        // create a new ImageView for each item referenced by the Adapter
        public View getView(int position, View grid, ViewGroup parent) {
            Detection detection = history.getHistoryItem(position);
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (grid == null) {
                grid = inflater.inflate(R.layout.grid_single, null);
            }
            TextView textView = (TextView)grid.findViewById(R.id.grid_text);
            ImageView imageView = (ImageView)grid.findViewById(R.id.grid_image);
            String label = detection.label;
            int sufpos = label.lastIndexOf("_part.jpg");
            if (sufpos > 0) label = label.substring(0, sufpos);
            sufpos = label.lastIndexOf('_');
            if (sufpos > 0) label = label.substring(0, sufpos);
            int labelpos = label.lastIndexOf('-');
            if (labelpos > 0) {
                labelpos += 2;
                int timepos = label.lastIndexOf('_', labelpos - 1);
                if (timepos > 0) {
                    String time =  label.substring(timepos, sufpos).replace('-', ':');
                    label = label.substring(0, timepos) + ' ' + time + ' ' + label.substring(sufpos);
                }
            }
            label = label.replace('_', ' ');
            textView.setText(label);
            imageView.setImageBitmap(detection.bitmap);
            return grid;
        }
    }

    private static final int PERMISSIONS_REQUEST = 1;

    private static final String PERMISSION_CAMERA = Manifest.permission.CAMERA;
    private static final String PERMISSION_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private static final String PERMISSION_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;

    @Override
    public void onRequestPermissionsResult(
            final int requestCode, final String[] permissions, final int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                resumeCamera();
            } else {
                label.setText("Access denied");
            }
        }
    }

    private boolean hasPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return checkSelfPermission(PERMISSION_CAMERA) == PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(PERMISSION_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(PERMISSION_LOCATION) == PackageManager.PERMISSION_GRANTED;
        } else {
            return true;
        }
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (shouldShowRequestPermissionRationale(PERMISSION_CAMERA) ||
                shouldShowRequestPermissionRationale(PERMISSION_STORAGE) ||
                shouldShowRequestPermissionRationale(PERMISSION_LOCATION)) {
                String message = "Camera, storage and location permissions are required";
                Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
                label.setText(message);
            }
            requestPermissions(
                    new String[] {PERMISSION_CAMERA, PERMISSION_STORAGE, PERMISSION_LOCATION},
                    PERMISSIONS_REQUEST
            );
        }
    }

    private static final int TWO_MINUTES = 1000 * 60 * 2;

    /** Determines whether one Location reading is better than the current Location fix
     * @param location  The new Location that you want to evaluate
     * @param currentBestLocation  The current Location fix, to which you want to compare the new one
     */
    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }
}
