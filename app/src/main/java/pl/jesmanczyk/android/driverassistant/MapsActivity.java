package pl.jesmanczyk.android.driverassistant;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.location.Location;
import android.location.LocationManager;
import android.preference.Preference;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap googleMap;
    private LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;

        DetectionRepository detectionRepository = new DetectionRepository();
        try {
            detectionRepository.load();
            int n = detectionRepository.getHistorySize();
            int i = 0;
            for (i = 0; i < n; i++) {
                DetectionRepository.Detection d = detectionRepository.getHistoryItem(i);
                if (d.gps != null) {

                    float ratio = d.bitmap.getHeight();
                    ratio /=  d.bitmap.getWidth();
                    float resultWidth = 80;
                    Matrix matrix = new Matrix();
                    //matrix.postScale(resultWidth / d.bitmap.getWidth(), resultWidth * ratio / d.bitmap.getHeight());
                    matrix.postScale(resultWidth / d.bitmap.getWidth(), resultWidth / d.bitmap.getHeight());
                    Bitmap bitmap;
                    bitmap = Bitmap.createBitmap(d.bitmap,0,0,  d.bitmap.getWidth(), d.bitmap.getHeight(), matrix, false);
                    this.googleMap.addMarker(new MarkerOptions()
                            .anchor(0.5f, 2.45f)
                            .position(new LatLng(d.gps.latitude, d.gps.longitude))
                            .icon(BitmapDescriptorFactory.fromBitmap(bitmap))
                            .title(d.label)
                    );
                    this.googleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(d.gps.latitude, d.gps.longitude))
                            .icon(BitmapDescriptorFactory.defaultMarker())
                    );
                }
            }
        } catch (Throwable e) {
            String message = "Failed to load detection history";
        }

        this.googleMap.getUiSettings().setCompassEnabled(true);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        this.googleMap.setMyLocationEnabled(true);
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
                new LatLng(location.getLatitude(), location.getLongitude()), 15
        );
        this.googleMap.animateCamera(cameraUpdate);
    }
}
