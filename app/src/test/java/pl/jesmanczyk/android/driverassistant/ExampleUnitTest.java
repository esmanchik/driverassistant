package pl.jesmanczyk.android.driverassistant;

import android.hardware.Camera;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void clearHistoryRemovesAllFilesOnValidFolderPath() throws Exception {
        ArrayList<File> files = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            File file = folder.newFile();
            files.add(file);
        }

        DetectionRepository.clearHistory(folder.getRoot().getAbsolutePath());

        assertTrue(files.size() > 0);
        for (File file: files) {
            assertFalse(file.exists());
        }
    }


    @Test(expected = RuntimeException.class)
    public void clearHistoryThrowsOnNonExistingFolderPath() throws Exception {
        DetectionRepository.clearHistory("notexists");
    }

    @Test
    public void updateCameraParametersSetsPictureAndPreviewSize() {
        CameraParametersAdapter params = new CameraParametersAdapter();
        params.supportedPictureSizes = createSizesList(640, 480, 800, 600, 1024, 768);
        params.supportedPreviewSizes = createSizesList(640, 480, 800, 600, 1024, 768);

        params = CameraParametersAdapter.calcImageSize(params, 0, 600);

        assertEquals(800, params.getPictureSize().width);
        assertEquals(600, params.getPictureSize().height);
        assertEquals(800, params.getPreviewSize().width);
        assertEquals(600, params.getPreviewSize().height);
    }


    @Test
    public void updateCameraParametersOnPictureAndPreviewSizesDoNotMatch() {
        CameraParametersAdapter params = new CameraParametersAdapter();
        params.supportedPictureSizes = createSizesList(640, 480, 800, 600, 1024, 768);
        params.supportedPreviewSizes = createSizesList(320, 200, 854, 640, 1280, 1024);

        params = CameraParametersAdapter.calcImageSize(params, 0, 600);

        assertEquals(800, params.getPictureSize().width);
        assertEquals(600, params.getPictureSize().height);
        assertEquals(854, params.getPreviewSize().width);
        assertEquals(640, params.getPreviewSize().height);
    }

    private List<CameraParametersAdapter.Size> createSizesList(int ...dims) {
        ArrayList<CameraParametersAdapter.Size> result = new ArrayList<CameraParametersAdapter.Size>();
        if (dims.length < 2) return result;
        for(int i = 0; i < dims.length; i += 2) {
            result.add(new CameraParametersAdapter.Size(dims[i], dims[i + 1]));
        }
        return result;
    }
}